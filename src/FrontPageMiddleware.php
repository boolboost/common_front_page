<?php

namespace Drupal\common_front_page;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * FrontPageMiddleware middleware.
 */
class FrontPageMiddleware implements HttpKernelInterface {

  /**
   * The kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * Constructs the FrontPageMiddleware object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   */
  public function __construct(HttpKernelInterface $http_kernel) {
    $this->httpKernel = $http_kernel;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {
    $uri_without_query = strtok($request->getRequestUri(), '?');
    $altered_query_params = empty($request->query->all()) ? '' : '?' . http_build_query($request->query->all());

    if ($uri_without_query == '/front') {
      return new RedirectResponse('/' . $altered_query_params);
    }

    return $this->httpKernel->handle($request, $type, $catch);
  }

}