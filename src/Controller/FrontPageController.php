<?php

namespace Drupal\common_front_page\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for book routes.
 */
class FrontPageController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [];
  }

}
